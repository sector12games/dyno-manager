// This module uses SpawnSync rather than ExecSync because ExecSync does not allow
// a way for us to trap and analyze stderr output, which the heroku cli uses. If
// the function you call writes to stderr, ExecSync will not intercept it in any
// way, and it will end up being output using your parent's stderr stream, which
// is typically the console. You can set the 'stdio' param of ExecSync to 'pipe',
// which will in fact ensure any stderr output goes to ExecSync, and is not passed
// along to the parent, but ExecSync does not provide a way to access this data.
// Any stderr output is simply swallowed.

const chalk = require('chalk');
const childProcess = require('child_process');

/**
 * Private function to execute a heroku terminal command.
 *
 * @private
 * @param {...string} args - Args to pass on the command line to the heroku command.
 * @returns {string} All output from the heroku command, joined together as a single string.
 */
function executeHerokuCommand(...args) {
    const result = childProcess.spawnSync('heroku', args);
    return result.output.reverse().join('');
}

/**
 * Verifies heroku is properly configured on your machine. This function called at the
 * start of every public function.
 *
 * @throws One of several possible errors if your local environment is not properly
 *      configured to use the heroku cli.
 * @returns {undefined}
 */
function verifyHerokuConfig() {
    let result;

    // Verify heroku cli (toolbelt) is installed
    result = childProcess.spawnSync('heroku', ['--version']);
    if (result.status !== 0) {
        throw new Error('You need to install heroku CLI/Toolbelt (download from their site)');
    }

    // Verify heroku cli is logged in
    result = childProcess.spawnSync('heroku', ['auth:whoami']);
    if (result.status !== 0) {
        throw new Error('You need to login using "heroku login"');
    }

    // Verify heroku remote is added
    result = childProcess.spawnSync('heroku', ['domains']);
    if (result.status !== 0) {
        throw new Error('You need to add a heroku git remote using ' +
            '"heroku git:remote -a my-heroku-app');
    }
}

/**
 * Scales one or more formation dynos (valid process types in your Procfile). To stop
 * a dyno, scale it to 0.
 *
 * @param {number} instanceCount - The number of instances to scale each process type to.
 * @param {...string} processTypes - A list of string process types that exist in your
 *      Procfile.
 * @returns {undefined}
 */
function scaleFormationDynos(instanceCount, ...processTypes) {
    verifyHerokuConfig();

    console.log(chalk.magenta(`Scaling ${processTypes.length} formation dyno(s)...`));
    processTypes.forEach((type) => {
        const result = executeHerokuCommand('ps:scale', `${type}=${instanceCount}`);
        console.log(result);
    });
}

/**
 * Starts one or more one-off dynos.
 *
 * @param {...string} processTypesOrCommands - A list of string process types that exist
 *      in your Procfile. Standard bash commands are accepted as well (passing 'bash' will
 *      result in 'heroku run:detached bash' being run).
 * @returns {undefined}
 */
function startOneOffDynos(...processTypesOrCommands) {
    verifyHerokuConfig();

    console.log(chalk.magenta(`Starting ${processTypesOrCommands.length} one-off dyno(s)...`));
    processTypesOrCommands.forEach((type) => {
        const result = executeHerokuCommand('run:detached', `${type}`);
        console.log(result);
    });
}

/**
 * Immediately stops all running one-off dynos. In general, you shouldn't be stopping
 * one-off dynos. Let them run to completion to maintain data consistency -
 * waitForAllOneOffDynosToComplete() is usually the better option.
 *
 * @returns {undefined}
 */
function abortAllOneOffDynos() {
    verifyHerokuConfig();

    const currentlyRunningDynos = getCurrentlyRunningOneOffDynos();
    console.log(chalk.magenta(`Stopping ${currentlyRunningDynos.length} one-off dyno(s)...`));
    currentlyRunningDynos.forEach((name) => {
        const result = executeHerokuCommand('stop', `${name}`);
        console.log(result);
    });
}

/**
 * Gets the list of all currently running one-off dynos.
 *
 * @returns {string[]} The list of all currently running one-off dynos. If no
 *      one-off dynos are running, an empty array will be returned.
 */
function getCurrentlyRunningOneOffDynos() {
    verifyHerokuConfig();

    // Get all 'run' processes currently executing. One-off dynos will always be
    // started up with a name similar to 'run.4835'
    const output = executeHerokuCommand('ps', 'run');

    // Extract the names of all running dynos
    const matches = output.match(/run\.[0-9]+/g);

    // Return an empty array if no matches
    return matches || [];
}

/**
 * Waits for all one-off dynos to complete. Dyno status will be checked every few
 * seconds and the promise will resolve when no more 'run' dyno types exist.
 *
 * @param {number} [timeoutMs=0] - The timeout (in ms). The default value of 0
 *      represents an infinite timeout.
 * @returns {Promise} - A promise that will resolve after no one-off dynos are
 *      running. The promise will reject if the provided timeout is exceeded.
 */
function waitForAllOneOffDynosToComplete(timeoutMs = 0) {
    verifyHerokuConfig();

    const startTime = Date.now();
    console.log(chalk.magenta('Waiting for all one-off dynos to complete...'));
    const promise = new Promise((resolve, reject) => {
        // Check to see if the promise is resolved every few seconds
        (function checkPromise() {
            // Check to see how many one-off dynos are currently running
            const currentlyRunning = getCurrentlyRunningOneOffDynos().length;

            // If none are running, fulfill the promise
            if (currentlyRunning === 0) {
                resolve('Success. No one-off dynos currently running.');
                return;
            }

            // If the timeout has been exceeded, reject the promise. A timeout
            // of 0 means an infinite timeout.
            if (timeoutMs > 0 && Date.now() - startTime > timeoutMs) {
                reject(Error('Timeout exceeded.'));
            } else {
                setTimeout(checkPromise, 3000);
            }
        })();
    });
    return promise;
}

module.exports = {
    verifyHerokuConfig,
    scaleFormationDynos,
    startOneOffDynos,
    abortAllOneOffDynos,
    getCurrentlyRunningOneOffDynos,
    waitForAllOneOffDynosToComplete,
};
