## dyno-manager

A module that allows for starting and stopping Heroku dynos.

---
#### setup

* In order to use this module, you must have a Heroku git url added to your repo
  and be signed into the Heroku CLI:  
  `heroku git:remote -a my-heroku-app-name`
* In order to run integration tests for this module, you'll have to:
  * Make sure you have an NPM authorization token environment variable set
    on your Heroku app, to authenticate to your private npm modules:  
    `heroku config:set NPM_TOKEN=00000000-0000-0000-0000-000000000000`
  * An NPM authorization token is automatically created in your .npmrc file
    when you run `npm login` from a command line. .npmrc is located:
    * `%UserProfile%\.npmrc`(windows)
    * `${HOME}/.npmrc` (osx)

---
#### troubleshooting

* ECONNRESET: socket hang up
  * This error likely means that you've got something blocked in your firewall.
    `heroku run:detached` is still usable, but `heroku run` will always fail.


