// A simple express server for integration testing the heroku DynoManager module.

var express = require('express')
var app = express()

// Heroku dynamically defines your app a port. The default for local node.js apps is 3000.
// Use heroku if defined, otherwise, use the default for running locally.
var port = process.env.PORT || 3000;

app.get('/', function (req, res) {
  res.send('Hello World!')
})

app.listen(port, function () {
  console.log('Example app listening on port 3000!')
})
