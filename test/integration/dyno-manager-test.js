/* global it, describe */
/* eslint prefer-arrow-callback: "off" */

const chai = require('chai');
const dynoManager = require('../../src/index');
const interceptIO = require('@sector12/intercept-io').interceptStdoutAndStderr;

describe('dyno-manager feature', function () {
    // Heroku integration tests take a little longer to run
    this.slow(5000);
    this.timeout(15000);

    // Ensure the machine running this code is properly configured to use the heroku cli
    describe('heroku configuration', function () {
        it('local machine is properly configured to use the heroku cli', function () {
            chai.assert.doesNotThrow(dynoManager.verifyHerokuConfig);
        });
    });

    // Formation tests
    describe('formation dyno tests', function () {
        it('should start single formation dyno', function () {
            const output = interceptIO(function () {
                dynoManager.scaleFormationDynos(1, 'web');
            });
            chai.assert.include(output.all, 'now running web at 1');
        });

        it('should stop single formation dyno', function () {
            const output = interceptIO(function () {
                dynoManager.scaleFormationDynos(0, 'web');
            });
            chai.assert.include(output.all, 'now running web at 0');
        });

        it('should start multiple formation dynos', function () {
            const output = interceptIO(function () {
                dynoManager.scaleFormationDynos(1, 'web', 'worker');
            });
            chai.assert.include(output.all, 'now running web at 1');
            chai.assert.include(output.all, 'now running worker at 1');
        });

        it('should stop multiple formation dynos', function () {
            const output = interceptIO(function () {
                dynoManager.scaleFormationDynos(0, 'web', 'worker');
            });
            chai.assert.include(output.all, 'now running web at 0');
            chai.assert.include(output.all, 'now running worker at 0');
        });
    });

    // One-off tests. Keep in mind that we cannot run more than one one-off free
    // dyno. For this reason, we won't have a test verifying multiple one-offs
    // are reported to be running, or a test to stop multiple one-offs.
    describe('one-off dyno tests', function () {
        it('should report no running one-off dynos', function () {
            const runningDynos = dynoManager.getCurrentlyRunningOneOffDynos();
            chai.assert.lengthOf(runningDynos, 0);
        });

        it('should start single one-off dyno', function () {
            // One-off dynos will always be started up with a name similar to 'run.4835'
            const output = interceptIO(function () {
                dynoManager.startOneOffDynos('oneOff1');
            });
            chai.assert.match(output.all, /done, run\.[0-9]+/);
        });

        it('should report single running one-off dyno', function () {
            const runningDynos = dynoManager.getCurrentlyRunningOneOffDynos();
            chai.assert.lengthOf(runningDynos, 1);
        });

        it('should stop single one-off dyno', function () {
            const output = interceptIO(function () {
                dynoManager.abortAllOneOffDynos();
            });

            const runningDynos = dynoManager.getCurrentlyRunningOneOffDynos();
            chai.assert.lengthOf(runningDynos, 0);
            chai.assert.include(output.all, '... done');
        });

        it('sould successfully wait for one-off dynos', function () {
            this.slow(30000);
            this.timeout(45000);

            let promise;
            const output = interceptIO(function () {
                // Start a one-off dyno
                dynoManager.startOneOffDynos('oneOff1');

                // Wait and verify output.
                promise = dynoManager.waitForAllOneOffDynosToComplete(35000).then(
                    function (result) {
                        chai.assert.deepEqual(result, 'Success. No one-off dynos currently running.');
                    },
                    function (error) {
                        throw new Error('Test was expected to succeed, but instead timed out.');
                    });
            });

            // Ensure the dyno was properly started as well
            chai.assert.match(output.all, /done, run\.[0-9]+/);

            return promise;
        });

        it('should timeout when waiting for one-off dynos', function () {
            this.slow(10000);
            this.timeout(15000);

            let promise;
            const output = interceptIO(function () {
                // Start a one-off dyno
                dynoManager.startOneOffDynos('oneOff1');

                // Wait and verify output
                promise = dynoManager.waitForAllOneOffDynosToComplete(5000).then(
                    function (result) {
                        throw new Error('Test was expected to timeout, but did not.');
                    },
                    function (error) {
                        chai.assert.deepEqual(error.message, 'Timeout exceeded.');
                    });
            });

            // Ensure the dyno was properly started as well
            chai.assert.match(output.all, /done, run\.[0-9]+/);

            return promise;
        });
    });
});
