/* global it, describe, beforeEach */
/* eslint prefer-arrow-callback: "off" */

const chai = require('chai');
const interceptIO = require('@sector12/intercept-io').interceptStdoutAndStderr;
const proxyquire = require('proxyquire');

// Parts of dynoManager need to be stubbed, so we'll require it using proxyquire
const childProcessStub = {};
const dynoManager = proxyquire('../../src/index', { child_process: childProcessStub });

// Mock child_process.spawnSync for testing
let mockCurrentlyRunningOneOffDynosArray = [];
childProcessStub.spawnSync = function (commandName, args) {
    let split;
    let processName;
    let instanceCount;

    // The second argument will have the process/command name and an optional
    // instance count (depending on the command being executed)
    if (args[1] !== undefined) {
        split = args[1].split('=');
        processName = split[0];
        instanceCount = split[1];
    }

    // Mock expected output based on args
    let result;
    switch (args[0]) {
        case 'ps:scale':
            result = `Scaling dynos... done, now running ${processName} at ${instanceCount}:Free`;
            break;
        case 'run:detached':
            result = `Running ${processName} on sector-12-test... done, run.3657 (Free)`;
            break;
        case 'stop':
            result = `Stopping ${processName} dyno on sector-12-test... done`;
            break;
        case 'ps':
            result = mockCurrentlyRunningOneOffDynosArray.reverse();
            break;
        default:
            result = 'Error. Args not supported.';
    }

    // The result from spawnSync should always be an arry of stdout/stderr values
    if (!Array.isArray(result)) {
        result = [result];
    }

    return {
        output: result,
        status: 0,
    };
};

describe('index-test.js', function () {
    // Reset any mocks that may be different for each test case
    beforeEach(function () {
        mockCurrentlyRunningOneOffDynosArray = [];
    });

    describe('scaleFormationDynos', function () {
        it('should scale zero formation dynos when "processTypes" is undefined', function () {
            const output = interceptIO(function () {
                dynoManager.scaleFormationDynos(1);
            });
            chai.assert.include(output.all, 'Scaling 0 formation dyno(s)...');
        });

        it('should scale single formation dyno', function () {
            const output = interceptIO(function () {
                dynoManager.scaleFormationDynos(1, 'web');
            });
            chai.assert.include(output.all, 'Scaling 1 formation dyno(s)...');
            chai.assert.include(output.all, 'Scaling dynos... done, now running web at 1:Free');
        });

        it('should scale multiple formation dynos', function () {
            const output = interceptIO(function () {
                dynoManager.scaleFormationDynos(1, 'web', 'worker');
            });
            chai.assert.include(output.all, 'Scaling 2 formation dyno(s)...');
            chai.assert.include(output.all, 'Scaling dynos... done, now running web at 1:Free');
            chai.assert.include(output.all, 'Scaling dynos... done, now running worker at 1:Free');
        });
    });

    describe('startOneOffDynos', function () {
        it('should start zero one-off dynos when "processTypesOrCommands" is undefined', function () {
            const output = interceptIO(function () {
                dynoManager.startOneOffDynos();
            });
            chai.assert.include(output.all, 'Starting 0 one-off dyno(s)...');
        });

        it('should start single one-off dyno', function () {
            const output = interceptIO(function () {
                dynoManager.startOneOffDynos('oneOff1');
            });
            chai.assert.include(output.all, 'Starting 1 one-off dyno(s)...');
            chai.assert.include(output.all, 'Running oneOff1 on sector-12-test... done, run.3657 (Free)');
        });

        it('should start multiple one-off dynos', function () {
            const output = interceptIO(function () {
                dynoManager.startOneOffDynos('oneOff1', 'oneOff2');
            });
            chai.assert.include(output.all, 'Starting 2 one-off dyno(s)...');
            chai.assert.include(output.all, 'Running oneOff1 on sector-12-test... done, run.3657 (Free)');
            chai.assert.include(output.all, 'Running oneOff2 on sector-12-test... done, run.3657 (Free)');
        });
    });

    describe('abortAllOneOffDynos', function () {
        it('should abort single one-off dyno', function () {
            // Take advantage of the fact that functions are late-bound by default
            // in javascript. No need to re-proxyquire.
            mockCurrentlyRunningOneOffDynosArray = ['run.1234'];

            const output = interceptIO(function () {
                dynoManager.abortAllOneOffDynos();
            });
            chai.assert.include(output.all, 'Stopping run.1234 dyno on sector-12-test... done');
        });

        it('should abort multiple one-off dynos', function () {
            // Take advantage of the fact that functions are late-bound by default
            // in javascript. No need to re-proxyquire.
            mockCurrentlyRunningOneOffDynosArray = ['run.1234', 'run.5678'];

            const output = interceptIO(function () {
                dynoManager.abortAllOneOffDynos();
            });
            chai.assert.include(output.all, 'Stopping run.1234 dyno on sector-12-test... done');
            chai.assert.include(output.all, 'Stopping run.5678 dyno on sector-12-test... done');
        });
    });

    describe('getCurrentlyRunningOneOffDynos', function () {
        it('should return empty array if no one-off dynos are running', function () {
            const result = dynoManager.getCurrentlyRunningOneOffDynos();
            chai.assert.deepEqual(result, []);
        });

        it('should return dyno names when one-off dynos are running', function () {
            // Take advantage of the fact that functions are late-bound by default
            // in javascript. No need to re-proxyquire.
            mockCurrentlyRunningOneOffDynosArray = ['run.1234', 'run.5678'];

            const result = dynoManager.getCurrentlyRunningOneOffDynos();
            chai.assert.includeMembers(result, ['run.1234', 'run.5678']);
        });
    });

    describe('waitForAllOneOffDynosToComplete', function () {
        this.slow(5000);
        this.timeout(10000);

        it('should resolve promise if no one-off dynos are running', function () {
            let promise;
            interceptIO(function () {
                promise = dynoManager.waitForAllOneOffDynosToComplete(1000).then(
                    function (result) {
                        chai.assert.deepEqual(result, 'Success. No one-off dynos currently running.');
                    },
                    function (error) {
                        throw new Error('Test was expected to succeed, but instead timed out.');
                    });
            });

            return promise;
        });

        it('should reject promise if timeout exceeded', function () {
            // Take advantage of the fact that functions are late-bound by default
            // in javascript. No need to re-proxyquire.
            mockCurrentlyRunningOneOffDynosArray = ['run.1234', 'run.5678'];

            let promise;
            interceptIO(function () {
                promise = dynoManager.waitForAllOneOffDynosToComplete(1000).then(
                    function (result) {
                        throw new Error('Test was expected to timeout, but did not.');
                    },
                    function (error) {
                        chai.assert.deepEqual(error.message, 'Timeout exceeded.');
                    });
            });

            return promise;
        });
    });
});
